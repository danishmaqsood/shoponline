import { View, Platform } from 'react-native';
import React from 'react';
import { createStackNavigator } from 'react-navigation';
import { Login } from '../screens/Login';
import { ForgotPassword } from '../screens/ForgotPassword';
import { Signup } from '../screens/SignUp';
import { HomeNavigator } from './HomeNavigator';

export const MainNavigator = createStackNavigator(
  {
    Login: Login,
    SignUp:  Signup,
    ForgotPassword: ForgotPassword,
    HomeNavigator: HomeNavigator
  },
  {
    initialRouteName: 'Login'
  }
)
